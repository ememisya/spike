/**
 * ITEC-471 SPIKE Assignment - by Erdem Memisyazici
 */
package edu.radford.itec471.spike.controllers;

import com.stormpath.sdk.servlet.form.Form;
import com.stormpath.sdk.servlet.mvc.RegisterController;
import com.stormpath.sdk.servlet.mvc.ViewModel;
import edu.radford.itec471.spike.dao.UserDAO;
import edu.radford.itec471.spike.entities.User;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Custom controller which overrides stormpath defaults to create a local
 * account alongside the stormpath account.
 */
public class CustomRegisterController extends RegisterController {

    @Autowired
    private UserDAO userDao;

    public void setUserDao(UserDAO userDao) {
        this.userDao = userDao;
    }

    @Override
    protected ViewModel onValidSubmit(HttpServletRequest req, HttpServletResponse resp, Form form) throws Exception {
        try {
            ViewModel result = super.onValidSubmit(req, resp, form);
            try {
                User user = new User(getValue(form, "email"), "Unspecified");
                userDao.save(user);
            } catch (Exception ex) {
                throw new Exception(
                        "Could not store user details in conjunction with account data.", ex);
            }
            return result;
        } catch (Exception exception) {
            throw exception;
        }
    }
}
