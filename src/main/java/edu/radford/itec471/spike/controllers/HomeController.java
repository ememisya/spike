/**
 * ITEC-471 SPIKE Assignment - by Erdem Memisyazici
 */
package edu.radford.itec471.spike.controllers;

import com.stormpath.sdk.account.Account;
import com.stormpath.sdk.servlet.account.AccountResolver;
import edu.radford.itec471.spike.dao.FoodItemDAO;
import edu.radford.itec471.spike.dao.UserDAO;
import edu.radford.itec471.spike.entities.FoodItem;
import edu.radford.itec471.spike.entities.User;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

    @Autowired
    private UserDAO userDao;

    @Autowired
    private FoodItemDAO foodItemDao;

    /**
     * Index page mapping
     */
    @RequestMapping("/")
    String index(HttpServletRequest request) {
        return "index";
    }

    /**
     * Home page mapping
     */
    @RequestMapping("/home")
    String home(HttpServletRequest request, Model model) {
        Account account = AccountResolver.INSTANCE.getAccount(request);
        User user = userDao.findByEmail(account.getEmail());
        if (user == null) {
            //If for some reason database was lost, recreate user details.
            user = new User(account.getEmail(), "Unspecified");
            userDao.save(user);
        }
        List<FoodItem> foodItems = foodItemDao.findByEmail(account.getEmail());
        model.addAttribute("foodItems", foodItems);
        model.addAttribute("userDetails", user);
        model.addAttribute("foodItem", new FoodItem());

        return "home";
    }
}
