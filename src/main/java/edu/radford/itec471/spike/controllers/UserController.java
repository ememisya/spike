/**
 * ITEC-471 SPIKE Assignment - by Erdem Memisyazici
 */
package edu.radford.itec471.spike.controllers;

import com.stormpath.sdk.servlet.account.AccountResolver;
import edu.radford.itec471.spike.dao.UserDAO;
import edu.radford.itec471.spike.entities.User;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserDAO userDao;

    /**
     * Updates user information
     *
     * @param req http request
     * @param id id of the user
     * @param gender gender of the user
     * @return view
     */
    @Valid
    @RequestMapping(method = RequestMethod.POST, value = "/update")
    public String updateUser(HttpServletRequest req, @NotNull long id, @NotEmpty String gender) {
        try {
            User existingUser = userDao.findOne(id);
            //if the user exists, and is the current user
            if (existingUser != null && existingUser.getEmail().equals(
                    AccountResolver.INSTANCE.getAccount(req).getUsername())) {
                existingUser.setGender(gender);
                userDao.save(existingUser);
            }
        } catch (Exception ex) {
            return "error";
        }
        return "redirect:/home";
    }
}
