/**
 * ITEC-471 SPIKE Assignment - by Erdem Memisyazici
 */
package edu.radford.itec471.spike.controllers;

import com.stormpath.sdk.servlet.account.AccountResolver;
import edu.radford.itec471.spike.dao.FoodItemDAO;
import edu.radford.itec471.spike.entities.FoodItem;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/fooditem")
public class FoodItemController {

    @Autowired
    private FoodItemDAO foodItemDao;

    /**
     * Updates food item information.
     *
     * @param req http request
     * @param foodItem foodItem data
     * @return
     */
    @Valid
    @RequestMapping(method = RequestMethod.POST, value = "/update")
    public String updateFoodItem(HttpServletRequest req,
            @ModelAttribute FoodItem foodItem) {
        try {
            FoodItem existingFoodItem = foodItemDao.findOne(foodItem.getId());
            //if the food item exists, and is the user's
            if (existingFoodItem != null && existingFoodItem.getEmail().equals(
                    AccountResolver.INSTANCE.getAccount(req).getUsername())) {
                //safeguard incase I remove validation rules later
                foodItem.setEmail(existingFoodItem.getEmail());
                foodItemDao.save(foodItem);
            }
        } catch (Exception ex) {
            return "error";
        }
        return "redirect:/home";
    }

    /**
     * Deletes food item information.
     *
     * @param req http request
     * @param foodItem foodItem data
     * @return
     */
    @Valid
    @RequestMapping(method = RequestMethod.POST, value = "/delete")
    public String deleteFoodItem(HttpServletRequest req,
            @ModelAttribute FoodItem foodItem) {
        try {
            FoodItem existingFoodItem = foodItemDao.findOne(foodItem.getId());
            //if the food item exists, and is the user's
            if (existingFoodItem != null && existingFoodItem.getEmail().equals(
                    AccountResolver.INSTANCE.getAccount(req).getUsername())) {
                //safeguard incase I remove validation rules later
                foodItemDao.delete(foodItem);
            }
        } catch (Exception ex) {
            return "error";
        }
        return "redirect:/home";
    }

    /**
     * Creates a new foodItem
     *
     * @param req http request
     * @param foodItem foodItem data
     * @return
     */
    @Valid
    @RequestMapping(value = "/new", method = RequestMethod.POST)
    String newFoodItem(HttpServletRequest req, @ModelAttribute FoodItem foodItem) {
        foodItem.setEmail(
                AccountResolver.INSTANCE.getAccount(req).getUsername());
        foodItemDao.save(foodItem);
        return "redirect:/home";
    }

}
