/**
 * ITEC-471 SPIKE Assignment - by Erdem Memisyazici
 */
package edu.radford.itec471.spike.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "fooditems")
/**
 * FoodItem entity representing a food item.
 */
public class FoodItem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    //The email of the user this food item belongs to.
    @NotEmpty
    private String email;

    @NotEmpty
    private String category;

    @NotEmpty
    private String meal;

    @NotEmpty
    private String name;

    @DecimalMin(value = "1")
    private Integer amount;

    public FoodItem() {
    }

    public FoodItem(long id, String email,
            String category, String meal, String name, Integer amount) {
        this.id = id;
        this.email = email;
        this.category = category;
        this.meal = meal;
        this.name = name;
        this.amount = amount;
    }

    public FoodItem(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getMeal() {
        return meal;
    }

    public void setMeal(String meal) {
        this.meal = meal;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

}
