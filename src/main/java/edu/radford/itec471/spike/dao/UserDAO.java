/**
 * ITEC-471 SPIKE Assignment - by Erdem Memisyazici
 */
package edu.radford.itec471.spike.dao;

import edu.radford.itec471.spike.entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface UserDAO extends CrudRepository<User, Long> {

    /**
     * Return user via email or null otherwise.
     *
     * @param email email
     * @return User instance.
     */
    public User findByEmail(String email);

}
