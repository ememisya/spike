/**
 * ITEC-471 SPIKE Assignment - by Erdem Memisyazici
 */
package edu.radford.itec471.spike.dao;

import edu.radford.itec471.spike.entities.FoodItem;
import edu.radford.itec471.spike.entities.User;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface FoodItemDAO extends CrudRepository<FoodItem, Long> {

    /**
     * Return food items via email or empty list otherwise.
     *
     * @param email email of the user
     * @return food items
     */
    public List<FoodItem> findByEmail(String email);

}
