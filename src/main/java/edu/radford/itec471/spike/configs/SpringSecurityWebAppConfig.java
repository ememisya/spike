/**
 * ITEC-471 SPIKE Assignment - by Erdem Memisyazici
 */
package edu.radford.itec471.spike.configs;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import static com.stormpath.spring.config.StormpathWebSecurityConfigurer.stormpath;
import edu.radford.itec471.spike.dao.UserDAO;
import edu.radford.itec471.spike.entities.User;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Application security configuration
 */
@Configuration
public class SpringSecurityWebAppConfig extends WebSecurityConfigurerAdapter {

    /**
     * {@inheritDoc}
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.apply(stormpath()).and()
                .authorizeRequests()
                .antMatchers("/home").fullyAuthenticated()
                .antMatchers("/user/**").fullyAuthenticated()
                .antMatchers("/*").permitAll();
    }

    /**
     * Custom user details service provided to spring to couple with
     * the account information received from stormpath servers
     */
    @Service
    class Users implements UserDetailsService {

        @Autowired
        final private UserDAO repo;

        @Autowired
        public Users(UserDAO repo) {
            this.repo = repo;
        }

        @Override
        public UserDetails loadUserByUsername(String username)
                throws UsernameNotFoundException {
            User user = repo.findByEmail(username);
            if (user == null) {
                return null;
            }
            List<GrantedAuthority> auth = AuthorityUtils
                    .commaSeparatedStringToAuthorityList("ROLE_USER");
            return new org.springframework.security.core.userdetails.User(
                    username, "[PROTECTED]",
                    auth);
        }

    }
}
