/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.radford.itec471.spike.configs;

import com.stormpath.sdk.lang.Assert;
import com.stormpath.sdk.servlet.form.DefaultField;
import com.stormpath.sdk.servlet.form.Field;
import com.stormpath.spring.config.AbstractStormpathWebMvcConfiguration;
import com.stormpath.spring.mvc.SpringController;
import edu.radford.itec471.spike.controllers.CustomRegisterController;
import edu.radford.itec471.spike.dao.UserDAO;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.mvc.Controller;

/**
 * This class overrides the default stormpath registration controller with
 * the custom one provided.  See: {@link CustomRegisterController}
 */
@Configuration
public class CustomRegisterControllerConfig extends AbstractStormpathWebMvcConfiguration {

    @Autowired
    private UserDAO userDao;

    @Bean
    @Override
    public Controller stormpathRegisterController() {
        if (idSiteEnabled) {
            return createIdSiteController(idSiteRegisterUri);
        }
        CustomRegisterController controller = new CustomRegisterController();
        controller.setUserDao(userDao);
        controller.setCsrfTokenManager(stormpathCsrfTokenManager());
        controller.setClient(client);
        controller.setEventPublisher(stormpathRequestEventPublisher());
        controller.setFormFields(toDefaultFields(stormpathRegisterFormFields()));
        controller.setLocaleResolver(stormpathLocaleResolver());
        controller.setMessageSource(stormpathMessageSource());
        controller.setAuthenticationResultSaver(stormpathAuthenticationResultSaver());
        controller.setUri(registerUri);
        controller.setView(registerView);
        controller.setNextUri(registerNextUri);
        controller.setLoginUri(loginUri);
        controller.setVerifyViewName(verifyView);
        controller.init();
        SpringController springController = new SpringController(controller);
        if (urlPathHelper != null) {
            springController.setUrlPathHelper(urlPathHelper);
        }
        return springController;
    }

    private List<DefaultField> toDefaultFields(List<Field> fields) {
        List<DefaultField> defaultFields = new ArrayList<>(fields.size());
        fields.stream().map((field) -> {
            Assert.isInstanceOf(DefaultField.class, field);
            return field;
        }).forEach((field) -> {
            defaultFields.add((DefaultField) field);
        });
        return defaultFields;
    }
}
