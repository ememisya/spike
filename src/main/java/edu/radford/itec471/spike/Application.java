/**
 * ITEC-471 SPIKE Assignment - by Erdem Memisyazici
 */
package edu.radford.itec471.spike;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    /**
     * Spring boot startup entry point
     *
     * @param args arguments (none required)
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
